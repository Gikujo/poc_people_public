<?php

namespace App\Form;

use App\Entity\Box;
use App\Entity\Role;
use App\Entity\Gender;
use App\Entity\People;
use App\Entity\Status;
use App\Repository\BoxRepository;
use App\Repository\RoleRepository;
use App\Repository\GenderRepository;
use App\Repository\PeopleRepository;
use App\Repository\StatusRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PeopleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'required' => false,
                'label' => "Prénom"
            ])
            ->add('lastname', TextType::class, [
                'required' => false,
                'label' => "Nom"
            ])
            ->add('fullname', TextType::class, [
                'label' => "Nom complet",
                'required' => false
            ])
            ->add('nickname', TextType::class, [
                'required' => false,
                'label' => "Pseudonyme"
            ])
            ->add('password', PasswordType::class, [
                'required' => false,
                'label' => "Mot de passe",
            ])
            ->add('birthday', BirthdayType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'required' => false,
                'label' => "Date de naissance"
            ])
            ->add('gender', EntityType::class, [
                'class' => Gender::class,
                'choice_label' => 'name',
                'query_builder' => function (GenderRepository $genderRepository) {
                    return $genderRepository->createQueryBuilder('g');
                },
                'label' => "Genre"
            ])
            ->add('roles', EntityType::class, [
                'class' => Role::class,
                'choice_label' => 'name',
                'multiple' => true,
                'query_builder' => function (RoleRepository $roleRepository) {
                    return $roleRepository->createQueryBuilder('r');
                },
                'label' => "Rôles"
            ])
            ->add('status', EntityType::class, [
                'class' => Status::class,
                'choice_label' => 'name',
                'query_builder' => function (StatusRepository $statusRepository) {
                    return $statusRepository->createQueryBuilder('s');
                },
                'label' => "Statuts"
            ])
            ->add('box', EntityType::class, [
                'required' => false,
                'class' => Box::class,
                'choice_label' => 'name',
                'query_builder' => function (BoxRepository $boxRepository) {
                    return $boxRepository->createQueryBuilder('b');
                },
                'label' => "Casier"
            ])
            ->add('inscription', DateTimeType::class, [
                'widget' => 'single_text',
                'required' => false,
                'label' => "Date d'inscription"
            ])
            // In this form, prescripteurs are supposed to be all people who don't
            // have one
            ->add('prescripteur', EntityType::class, [
                'class' => People::class,
                'choice_label' => 'nickname',
                'query_builder' => function (PeopleRepository $peopleRepository) {
                    return $peopleRepository->createQueryBuilder('p')
                        ->join('p.roles', 'r')
                        ->where('r.name = :name')
                        ->setParameter("name", "prescripteur");
                },
                'label' => "Prescripteur"
            ])
            ->add('note', TextareaType::class, [
                'required' => false,
                'label' => "Note"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        // $box = $people->isRecipient() ? $people->getBox() : null;
        // $constraints = [
        //     'box' => $box
        // ];

        $resolver->setDefaults([
            // 'constraints' => $constraints,
            'data_class' => People::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'csrf_token_id'   => 'people_item',
        ]);
    }
}
