<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use App\Repository\PeopleRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PeopleRepository::class)]
#[ORM\Table(name: "People")]
class People
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['api-people', 'api-people-all', 'api-open'])]
    private ?int $id = null;

    #[Groups(['api-people-all'])]
    #[ORM\Column(name: "firstname", type: Types::STRING, length: 255, nullable: true)]
    private ?string $firstname = null;

    #[Groups(['api-people-all', 'api-open'])]
    #[ORM\Column(name: "fullname", type: Types::STRING, length: 255, nullable: true)]
    private ?string $fullname = null;

    #[Groups(['api-people-all'])]
    #[ORM\Column(name: "inscription", type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $inscription = null;

    #[Groups(['api-people-all'])]
    #[ORM\Column(name: "lastname", type: Types::STRING, length: 255, nullable: true)]
    private ?string $lastname = null;

    #[Groups(['api-people', 'api-people-all'])]
    #[ORM\Column(name: "nickname", type: Types::STRING, length: 255, nullable: true)]
    private ?string $nickname = null;

    #[ORM\Column(name: "password", type: Types::STRING, length: 255, nullable: true)]
    private ?string $password = null;

    #[Groups(['api-people-all'])]
    #[ORM\Column(name: "birthday", type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $birthday = null;

    #[Groups(['api-people-all'])]
    #[ORM\Column(name: "note", type: Types::TEXT, nullable: true)]
    private ?string $note = null;

    #[ORM\OneToMany(mappedBy: 'people', targetEntity: Contact::class)]
    private Collection $contacts;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn("gender_id", referencedColumnName: "id", onDelete: "SET NULL")]
    private ?Gender $gender = null;

    #[Groups(['api-people'])]
    #[ORM\OneToMany(mappedBy: 'people', targetEntity: Open::class, fetch: "EXTRA_LAZY")]
    #[OrderBy(["openDate" => "DESC"])]
    private Collection $opens;

    #[ORM\JoinTable(name: 'people_role')]
    #[ORM\JoinColumn(name: 'people_id', referencedColumnName: 'id', onDelete: "SET NULL")]
    #[ORM\InverseJoinColumn(name: 'role_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: Role::class, fetch: 'EXTRA_LAZY')]
    private ?Collection $roles;

    #[ORM\OneToMany(mappedBy: 'people', targetEntity: StatusHistory::class)]
    #[ORM\JoinColumn(name: 'prescripteur_id', referencedColumnName: 'id')]
    #[OrderBy(["changeDate" => "DESC"])]
    private ?Collection $statusHistories;

    #[Groups(['api-people-all'])]
    #[ORM\ManyToOne(targetEntity: "Status", inversedBy: 'people')]
    #[ORM\JoinColumn('status_id', referencedColumnName: "id", onDelete: "SET NULL")]
    private ?Status $status;

    #[Groups(['api-people-all'])]
    #[ORM\ManyToOne(targetEntity: self::class)]
    #[ORM\JoinColumn("prescripteur_id", referencedColumnName: "id", onDelete: "SET NULL")]
    private ?self $prescripteur = null;

    #[Groups(['api-people', 'api-people-all', 'api-open'])]
    #[ORM\ManyToOne(targetEntity: "Box", inversedBy: 'people')]
    #[ORM\JoinColumn(name: "box_id", referencedColumnName: "id", nullable: true, onDelete: "SET NULL")]
    private ?Box $box = null;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->opens = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->statusHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(?string $fullname): void
    {
        $this->fullname = $fullname;
    }

    public function getInscription(): ?\DateTimeInterface
    {
        return $this->inscription;
    }

    public function setInscription(?\DateTimeInterface $inscription): void
    {
        $this->inscription = $inscription;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): void
    {
        $this->lastname = $lastname;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(?string $nickname): void
    {
        $this->nickname = $nickname;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): void
    {
        $this->birthday = $birthday;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): void
    {
        $this->note = $note;
    }

    /**
     * @return Collection<int, Contact>
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): void
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
            $contact->setPeople($this);
        }
    }

    public function removeContact(Contact $contact): void
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getPeople() === $this) {
                $contact->setPeople(null);
            }
        }
    }

    public function getGender(): ?Gender
    {
        return $this->gender;
    }

    public function setGender(?Gender $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return Collection<int, Open>
     */
    public function getOpens(): Collection
    {
        return $this->opens;
    }

    public function addOpen(Open $open): void
    {
        if (!$this->opens->contains($open)) {
            $this->opens->add($open);
            $open->setPeople($this);
        }
    }

    public function removeOpen(Open $open): void
    {
        if ($this->opens->removeElement($open)) {
            // set the owning side to null (unless already changed)
            if ($open->getPeople() === $this) {
                $open->setPeople(null);
            }
        }
    }

    /**
     * @return Collection<int, Role>
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(Role $role): void
    {
        if (!$this->roles->contains($role)) {
            $this->roles->add($role);
        }
    }

    public function removeRole(Role $role): void
    {
        $this->roles->removeElement($role);
    }

    /**
     * @return Collection<int, StatusHistory>
     */
    public function getStatusHistories(): Collection
    {
        return $this->statusHistories;
    }

    public function addStatusHistory(StatusHistory $statusHistory): void
    {
        if (!$this->statusHistories->contains($statusHistory)) {
            $this->statusHistories->add($statusHistory);
            $statusHistory->setPeople($this);
        }
    }

    public function removeStatusHistory(StatusHistory $statusHistory): void
    {
        if ($this->statusHistories->removeElement($statusHistory)) {
            // set the owning side to null (unless already changed)
            if ($statusHistory->getPeople() === $this) {
                $statusHistory->setPeople(null);
            }
        }
    }

    public function isRecipient()
    {
        return $this->getStatus()->getName() === "Bénéficiaire";
    }

    /**
     * @param People $people
     * This method checks if the people to persist is recipient.
     * If it's not, then its box is set to null
     */
    public function prePersist(People $people): void
    {
        if ($people->getStatus()->getName() !== "Bénéficiaire") {
            $people->setBox(null);
        }
    }

    /**
     * @param People $people
     * This method checks if the people to update is recipient.
     * If it's not, then its box is set to null
     */
    public function preUpdate(People $people): void
    {
        if ($people->getStatus()->getName() !== "Bénéficiaire") {
            $people->setBox(null);
        }
    }

    /**
     * @param ExecutionContextInterface $context
     *
     * This method is called when adding or updating a people.
     * If it's  recipient and a box is not set, it is not persisted, and an error message
     * is shown on the form.
     */
    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->getStatus()->getName() === "Bénéficiaire"
            && $this->getBox() === null
        ) {
            $context->buildViolation("Un usager bénéficiaire doit avoir un casier.")
                ->atPath('box')
                ->addViolation();
        }
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }
    public function setStatus(Status $status): void
    {
        $this->status = $status;
    }

    public function getPrescripteur(): ?self
    {
        return $this->prescripteur;
    }

    public function setPrescripteur(?self $prescripteur): static
    {
        $this->prescripteur = $prescripteur;

        return $this;
    }

    public function getBox(): ?Box
    {
        return $this->box;
    }

    public function setBox(?Box $box): static
    {
        $this->box = $box;

        return $this;
    }
}
