<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/app', name: 'app_')]
class AppController extends AbstractController
{
    #[Route('/prescripteurs', name: 'prescripteurs')]
    public function showPrescripteurs(): Response
    {
        return $this->render('app/prescripteurs/index.html.twig', [
            'page_name' => 'Prescripteurs',
            'controller_name' => 'AppController',
        ]);
    }

    #[Route('/people', name: 'people')]
    public function showPeople(): Response
    {
        $params  = [
            'controller_name' => 'AppController',
            'page_name' => 'Usagers',
        ];
        if (isset($_GET["message"])) {
            $params["message"] = $_GET["message"];
        }
        return $this->render('app/people/index.html.twig', $params);
    }

    #[Route('/opens', name: 'opens')]
    public function showOpens(): Response
    {
        return $this->render('app/opens/index.html.twig', [
            'controller_name' => 'AppController',
            'page_name' => 'Passages',
        ]);
    }
}
