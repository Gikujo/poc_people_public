<?php

namespace App\Controller\Crud;

use App\Entity\Open;
use App\Entity\People;
use App\Entity\Contact;
use App\Form\PeopleType;
use App\Entity\StatusHistory;
use App\ResponseText\ResponseText;
use App\Repository\PeopleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/people')]
class PeopleController extends AbstractController
{
    public function getPagination(
        $peopleRepository,
        $paginator,
        $request
    ) {
        $pagination = $paginator->paginate(
            $peopleRepository->findAll(),
            $request->query->getInt('page', 1), /*page number*/
            25 /*limit per page*/
        );

        return $pagination;
    }

    #[Route('/', name: 'app_admin_people_index', methods: ['GET'])]
    public function index(
        PeopleRepository $peopleRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $pagination = $this->getPagination($peopleRepository, $paginator, $request);

        return $this->render('people/index.html.twig', [
            'people' => $pagination,
            'page_name' => "Liste des personnes",
        ]);
    }

    #[Route('/new', name: 'app_admin_people_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        $person = new People();

        $form = $this->createForm(PeopleType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $person->prePersist($person);
            $entityManager->persist($person);
            $entityManager->flush();

            return $this->redirectToRoute(
                'app_people',
                ["message" => ResponseText::CREATED_OK],
                Response::HTTP_SEE_OTHER
            );
        }

        return $this->render('people/new.html.twig', [
            'person' => $person,
            'form' => $form,
            'page_name' => "Enregistrer une personne",
        ]);
    }

    #[Route('/{id}', name: 'app_admin_people_show', methods: ['GET'])]
    public function show(People $person): Response
    {
        return $this->render('people/show.html.twig', [
            'person' => $person,
            'page_name' => "Personne : " . $person->getNickname(),
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_people_edit', methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        People $person,
        EntityManagerInterface $entityManager,
    ): Response {
        $form = $this->createForm(PeopleType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $person->preUpdate($person);
            $entityManager->flush();

            return $this->redirectToRoute(
                'app_people',
                ["message" => ResponseText::EDITED_OK],
                Response::HTTP_SEE_OTHER
            );
        }

        return $this->render('people/edit.html.twig', [
            'person' => $person,
            'form' => $form,
            'page_name' => "Éditer une personne : " . $person->getNickname(),
        ]);
    }

    #[Route('/{id}', name: 'app_admin_people_delete', methods: ['POST'])]
    public function delete(
        Request $request,
        People $person,
        EntityManagerInterface $entityManager
    ): Response {
        dump('delete function called');
        if ($this->isCsrfTokenValid('delete' . $person->getId(), $request->request->get('_token'))) {
            // In order to avoid integrity constraints violations,
            // We have to first set concerned opens's and statusHistories's peoples to "null"
            // Those entities boxes have the "onDelete : 'SET NULL'" parameter, but
            // This is only about the db, not the Doctrine Entities.

            $opens = $entityManager->getRepository(Open::class)->findBy(['people' => $person]);
            $contacts = $entityManager->getRepository(Contact::class)->findBy(['people' => $person]);
            $statusHistories = $entityManager->getRepository(StatusHistory::class)->findBy(['people' => $person]);
            foreach ($opens as $open) {
                $open->setPeople(null);
            }
            foreach ($contacts as $eachContact) {
                $eachContact->setPeople(null);
            }
            foreach ($statusHistories as $eachStatusHistory) {
                $eachStatusHistory->setPeople(null);
            }
            $entityManager->flush();

            // Now, we can safely remove the person
            $entityManager->remove($person);
            $entityManager->flush();
        }

        return $this->redirectToRoute(
            'app_people',
            ["message" => ResponseText::DELETED_OK],
            Response::HTTP_SEE_OTHER
        );
    }
}
