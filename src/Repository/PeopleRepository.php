<?php

namespace App\Repository;

use App\Entity\People;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<People>
 *
 * @method People|null find($id, $lockMode = null, $lockVersion = null)
 * @method People|null findOneBy(array $criteria, array $orderBy = null)
 * @method People[]    findAll()
 * @method People[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeopleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, People::class);
    }

    /**
     * @param array      $criteria who contains People target properties
     * @param null|array $orderBy  optionnal to order accoring to a People property
     * @param null|int   $limit
     * @param null|int   $offset
     *
     * @return People[] Returns an array of People objects
     */
    public function findByProperties(
        // Initialize the criterias
        ?int $roleId = null,
        ?int $statusId = null,
        bool $withBoxes = false,
    ): array {
        $queryBuilder = $this->createQueryBuilder('p');

        // Check if criterias are set and if they are, make the DQL request
        if ($roleId != null) {
            $queryBuilder
                ->join('p.roles', 'r')
                ->andwhere('r.id = :id')
                ->setParameter('id', $roleId);
        }
        if ($statusId != null) {
            $queryBuilder
                ->andWhere('p.status = :status')
                ->setParameter('status', $statusId);
        }
        if ($withBoxes) {
            $queryBuilder
                ->orderBy('p.box', 'ASC');
        }

        return $queryBuilder->getQuery()->getResult();
    }

    //    /**
    //     * @return People[] Returns an array of People objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?People
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
