
export default {
    name: 'commontable',
    template: `
        <table class="table table-striped">
            <thead>
                <slot name="header"></slot>
            </thead>
            <tbody>
                <slot name="body"></slot>
            </tbody>
        </table>
    `,
};