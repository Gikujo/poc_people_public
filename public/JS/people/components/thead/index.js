
export default {
    name: 'commonthead',
    props: [
        'vrecipientStatus',
        'vcolumnsclass'],
    template: `
    <tr>
        <th class="th-sm align-middle">Sexe</th>
        <th class="th-sm align-middle">Pseudonyme</th>
        <th v-bind:class="'d-custom th-sm align-middle ' + vcolumnsclass">Nom complet</th>
        <th v-bind:class="'d-custom th-sm align-middle ' + vcolumnsclass">Âge</th>
        <th class="th-sm align-middle filter recipient" v-if="currentStatusId == vrecipientStatus">Casier</th>
        <th class="th-sm align-middle d-md-table-cell">Prescripteur</th>
        <th class="th-sm align-middle filter waitlist d-md-table-cell" v-if="currentStatusId == waitingListStatus">Attente depuis le</th>
        <th class="th-sm align-middle filter recipient d-md-table-cell" v-if="currentStatusId == vrecipientStatus">Absence</th>
        <th class="th-sm align-middle filter norecipient d-md-table-cell" v-if="currentStatusId == noLongerRecipientStatus">Dernier passage</th>
        <th class="d-md-table-cell th-sm align-middle filter nowaiting" v-if="currentStatusId == noLongerOnWaitingListStatus">Désinscrit le</th>
        <th v-bind:class="'d-custom th-sm align-middle filter recipient ' + vcolumnsclass" v-if="currentStatusId == vrecipientStatus">Bénéficiaire <br> depuis</th>
        <th class="th-sm align-middle">Actions</th>
    </tr>
    `
};