export default {
    name: 'commontbody',
    props: ['people',
        'vcolumnsclass'],
    emits: ['showdetails'],
    methods: {
        intern_showdetails(people) {
            this.$emit('showdetails', people)
        }
    },
    template: `
<!--***** line for ecah people *****--> 
<tr>
    <!--***** genders *****-->
    <!-- if gender female -->
    <td scope="row" v-if="people.gender == 'Féminin'">
        <button class="btn bg-danger-subtle">
            <i class="bi bi-gender-female"> F</i>
        </button>
    </td>
    <!-- if gender male -->
    <td scope="row" v-else-if="people.gender == 'Masculin'">
        <button class="btn bg-primary-subtle">
            <i class="bi bi-gender-male"> H</i>
        </button>
    </td>
    <!-- if other gender -->
    <td scope="row" v-else>
        <p v-if="people.gender">{{ people.gender }}</p>
    </td>
    <!--***** people name *****-->
    <td @click="intern_showdetails(people)" scope="row" v-bind:id="people.id">
        <p class="pointer">
            {{ people.name }}
        </p>
    </td>
    <!--***** people fullname (hidden when vcolumnsclass = "d-none") *****-->
    <td v-bind:class="'d-custom ' + vcolumnsclass">
        <p v-if="people.fullname">
            {{ people.fullname }}
        </p>
    </td>
    <!--***** people age (hidden when vcolumnsclass = "d-none") *****-->
    <td v-bind:class="'d-custom ' + vcolumnsclass">
        <p v-if="people.age">
            {{ people.age }} ans
        </p>
    </td>
    <!--***** people box number (shown only for recipients) *****-->
    <td class="filter recipient" v-if="people.box && currentStatusId == recipientStatus">
        {{ people.box.name }}
    </td>
    <!-- ***** people prescripteur's name ***** -->
    <td class="d-md-table-cell">
        <p v-if="people.prescripteur">
            {{ people.prescripteur.name }}
        </p>
    </td>
    <!--***** people waitingDate (shown only for waiting List) ***** *****-->
    <td class="d-md-table-cell filter waitlist" v-if="currentStatusId == waitingListStatus"> 
        <span v-if="people.waitingDate" class="p-1 rounded text-blanc bg-gradient bg-secondary">
            {{ people.waitingDate }}
        </span>
    </td>
    <!--***** people absence duration (only for recipients) *****-->
    <td class="d-md-table-cell filter recipient" v-if="currentStatusId == recipientStatus"> 
        <span v-if="people.absentSince" v-bind:class="'p-1 rounded text-blanc bg-gradient bg-' + people.absenceColor">
            {{ people.absentSince }} jours
        </span>
    </td>
    <!--***** people last open date (ony for no more recipients) *****-->
    <td class="d-md-table-cell filter norecipient" v-if="currentStatusId == noLongerRecipientStatus"> 
        <span v-if="people.waitingDate" class="p-1 rounded text-blanc bg-gradient bg-secondary">
            {{ people.lastOpenDate }}
        </span>
    </td>
    <!--***** people desincription date (ony for no more on waiting list) *****-->
    <td class="d-md-table-cell filter nowaiting" v-if="currentStatusId == noLongerOnWaitingListStatus"> 
        <span v-if="people.waitingDate" class="p-1 rounded text-blanc bg-gradient bg-secondary">
            {{ people.noWaitingDate }}
        </span>
    </td>
    <!--***** people date when  last time became a recipient (ony for recipients) *****-->
    <td v-bind:class="'d-custom filter recipient ' + vcolumnsclass" v-if="currentStatusId == recipientStatus">
        {{ people.lastRecipientDate }}
    </td>
    <!--***** Actions area *****-->
    <td class="th-sm align-middle">
        <a v-bind:href="'/admin/people/' + people.id + '/edit'" type="button" class="btn btn-info">
            <i class="bi bi-pencil"></i>
        </a>
    </td>
</tr>
<tr v-if="people.showdetails"></tr>
<tr v-if="people.showdetails">
        <td colspan="9">
            <table class="w-100">
                <thead>
                <tr>
                    <th class="bg-secondary-subtle th-sm w-50" scope = "col">Profil <span class="badge text-bg-secondary"> id: {{ people.id }}</span></th>
                    <th class="bg-secondary-subtle th-sm w-50" scope = "col">Historique</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td scope = "row">
                        <ul>
                            <li>
                                1ère inscription: {{ people.inscription }}
                            </li>
                            <li>
                                sexe : {{ people.gender.name }}
                            </li>
                        </ul>
                                <a v-bind:href="'/admin/contact/new?people=' + people.id"
                        type="button"
                        title="Ajouter contact à cette personne"
                        class="btn btn-success mt-3">
                            <i class="bi bi-plus-circle me-2"></i>Ajouter contact
                        </a>
                        <ul>
                            <li v-for="contact in people.contacts" :key="contact.id">
                                {{ contact.contactType.name }} : {{ contact.content }}
                            </li>
                        </ul>
                    </td>
                    <td scope = "row">
                        <li v-for="sh in people.statusHistories" :key="sh.id">
                            {{sh.changeDate}} : {{sh.status.name}}
                        </li>
                    </td>
                </tr>
                </tbody>
                <thead>
                <tr>
                    <th class="bg-secondary-subtle th-sm w-50" scope = "col">
                        Prescripteur <span class="badge text-bg-secondary">id: {{ people.prescripteur.id }}</span>
                    </th>
                    <th class="bg-secondary-subtle th-sm w-50" scope = "col">
                        Note
                    </th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope = "row">
                            {{ people.prescripteur.fullname }} <br />
                            
                            <a v-bind:href="'/admin/contact/new?people=' + people.prescripteur.id"
                            type="button"
                            title="Ajouter contact au prescripteur"
                            class="btn btn-success mt-3">
                                <i class="bi bi-plus-circle me-2"></i>Ajouter contact
                            </a>
                            <ul>
                                <li v-for="contact in people.prescripteur.contacts" :key="contact.id">
                                    {{ contact.contactType.name }} : {{ contact.content}}
                                </li>
                            </ul>
                        </td>
                        <td scope = "row">
                            <div class="form-floating" v-if="people.note != '' ">
                                <textarea height="" class="form-control">{{ people.note }}</textarea>
                                <label for="floatingTextarea">Note</label>
                                </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>`
};