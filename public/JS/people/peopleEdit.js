/*********************************************************************************
 ************************ VUE.JS PEOPLE EDIT JAVASCRIPT **************************
 ********************************************************************************/
const { createApp } = Vue;

const app = Vue.createApp({
    data() {
        return {
            boxInput: null,
            statusInput: null,
        }
    },
    mounted() {
        this.boxInput = document.querySelector('#people_box');
        this.statusInput = document.querySelector('#people_status');
        this.showOrNotBoxInput();
        this.statusInput.addEventListener('change', () => {
            this.showOrNotBoxInput();
        });
    },
    methods: {
        /**
         * This method affects the front of the form: if other field than "Bénéficiaiire"
         *  is selected in the status Input, box's input will be hidden.
         */
        showOrNotBoxInput() {
            for (let index = 0; index < this.statusInput.children.length; index++) {
                const element = this.statusInput[index];
                if (element.value === this.statusInput.value) {
                    if (element.textContent === "Bénéficiaire") {
                        if (this.boxInput.parentNode.classList.contains('d-none')) {
                            this.boxInput.parentNode.classList.remove('d-none');
                        }
                    } else {
                        if (!this.boxInput.parentNode.classList.contains('d-none')) {
                            this.boxInput.parentNode.classList.add('d-none');
                        }
                    }
                }
            }
        }
    }
});

app.mount('form');