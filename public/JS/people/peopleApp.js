// ************************** COMPONENTS IMPORTS *********************************
import commontable from './components/table/index.js';
import commonthead from './components/thead/index.js';
import commontbody from './components/tbody/index.js';

import { ApiService } from '../../services/apiService.js';

/*********************************************************************************
 ****************************** VUE.JS PEOPLE APP ********************************
 ********************************************************************************/
const { createApp } = Vue;

const app = Vue.createApp({
    data() {
        return {
            message: 'Afficher les usagers',
            peoples: [],
            status: [],
            showColumns: false,
            currentStatusId: 0,
            usagerId: 4,
            waitingListStatus: 1,
            recipientStatus: 3,
            noLongerRecipientStatus: 4,
            noLongerOnWaitingListStatus: 5,
            ApiService: new ApiService(),
            columnsClass: 'd-none',
        }
    },
    mounted() {
        this.loadPeoples(this.usagerId, this.recipientStatus);
        this.loadStatus();
    },
    methods: {
        /** 
         * @param {int} roleId 
         * @param {int} statusId
         * 
         * This method set this.peoples once loaded from the API
         */
        loadPeoples(roleId, statusId) {
            this.peoples = [];
            this.ApiService.loadPeoples(roleId, statusId)
                .then(res => {
                    let list = res.data.peoples;
                    list.forEach(people => {
                        people.prescripteur = this.setPrescripteur(people);
                        this.peoples.push(people);
                        people.absenceColor = this.setAbsenceColor(people);
                        people.showdetails = false;
                    });
                    this.currentStatusId = statusId;
                    this.filterColumns(this.currentStatusId);
                })
                .catch(err => {
                    console.error(err);
                });
        },
        /**
         * This method set this.status once loaded from the API
         *  in order to display them in the dropdown selection
         */
        loadStatus() {
            axios.get('/api/status/list')
                .then(res => {
                    res.data.forEach(statut => {
                        this.status.push(statut);
                    });
                })
                .catch(err => {
                    console.error(err);
                })
        },
        /**
         * @param {Object} people 
         * @returns {Object} formatted people.prescripteur
         */
        setPrescripteur(people) {
            if (!people.prescripteur) {
                people.prescripteur = {
                    name: ""
                };
            }
            return people.prescripteur;
        },
        /**
         * @param {Object} people 
         * @returns {string} Bootstrap color class in order to color the
         * "absent depuis" section in the people page
         */
        setAbsenceColor(people) {
            if (people.absentSince < 0 || people.absentSince == null) {
                return "primary-subtle";
            }
            if (people.absentSince < 7) {
                return "success-subtle";
            }
            if (people.absentSince < 14) {
                return "warning-subtle";
            }
            return "danger-subtle";
        },
        /**
         * This method is used when the "Afficher toutes les colonnes" button is clicked
         * @sets this.showColumns in order to display the hidden columns
         */
        toggleColumns() {
            this.showColumns = !this.showColumns;

            if (this.showColumns) {
                this.columnsClass = "";
            } else {
                this.columnsClass = "d-none";
            }
        },
        showdetails(people) {
            this.peoples.forEach(element => {
                if (element.showdetails && element != people) {
                    element.showdetails = false;
                }
            });
            people.showdetails = !people.showdetails;
        },
        filterColumns(statusId) {
            // if statusId is not a number, convert it into number
            if (typeof (statusId) != "number") {
                statusId = Number(statusId);
            }
            switch (statusId) {
                case this.recipientStatus:
                    // display recipientStatus columns
                    this.display('recipient');
                    break;
                case this.waitingListStatus:
                    // display waitingListStatus columns
                    this.display('waitlist');
                    break;
                case this.noLongerRecipientStatus:
                    // display noLongerRecipientStatus columns
                    this.display('norecipient');
                    break;
                case this.noLongerOnWaitingListStatus:
                    // display noLongerOnWaitingListStatus columns
                    this.display('nowaiting');
                    break;
                default:
                    // nothing
                    break;
            }
        },
        display(classname) {
            // select the old "style" tag
            let styleTag = document.getElementById('dynamictag');
            // delete it
            if (!!styleTag) {
                styleTag.remove();
            }
            // create a CSS rule to display the elements with classname
            let displayRule = `.${classname} {display: table-cell !important;}`;
            // create a CSS rule to drop the others
            let dropRule = `.filter:not(.${classname}) {display: none !important;}`;
            // create a new "style" tag
            let newStyleTag = document.createElement('style');
            // add the CSS rules
            newStyleTag.appendChild(document.createTextNode(displayRule));
            newStyleTag.appendChild(document.createTextNode(dropRule));
            newStyleTag.setAttribute('id', 'dynamictag');
            // add the "style" tag in the "head" element
            document.head.appendChild(newStyleTag);
        }
    }
});

// *********************** ADD THE COMPONENTS TO THE APP *************************
app.component('commontable', commontable);
app.component('commonthead', commonthead);
app.component('commontbody', commontbody);

app.mount('#app');