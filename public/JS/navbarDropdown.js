//***************  HELP THE BOOTSRAP DROPDOWN NAVBAR COMPONENT *******************
function HelpDropDown() {
    let dropdown = document.querySelector('.dropdown');
    let dropdownMenu = document.querySelector('.dropdown-menu');
    let show = false;

    dropdown.addEventListener('click', () => {
        show = !show;
        if (show) {
            dropdown.classList.add('show');
            dropdownMenu.classList.add('show');
            dropdown.setAttribute('aria-expanded', 'true');
            dropdownMenu.setAttribute('data-bs-popper', 'static');
        } else {
            dropdown.classList.remove('show');
            dropdownMenu.classList.remove('show');
            dropdown.setAttribute('aria-expanded', 'false');
            dropdownMenu.removeAttribute('data-bs-popper');
        }
    });
}

HelpDropDown();
// *******************************************************************************
