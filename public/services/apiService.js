export class ApiService {
    constructor() {
        this.peoples = [];
    }

    /**
     * This method fetches the People API endpoint
     * @param {int} roleId
     * @param {int} statusId
     * @return JSON object of peoples.
     */
    loadPeoples(roleId = null, statusId = null) {
        return axios.get('/api/people/list', {
            params: {
                'role': roleId,
                'status': statusId
            }
        })
    }
}
